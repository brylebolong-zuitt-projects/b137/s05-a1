package batch137.bolong.s05a1;

import java.util.ArrayList;

public class Contact {

    public String name;
    public String phoneNumber;
    public String address;




    //Empty Constructor
    public Contact(){}

        // Parametrized constructor

     public Contact(String name, String phoneNumber, String address ) {
            this.name = name;
            this.phoneNumber = phoneNumber;
            this.address = address;


        }
        // Getters
        public String  getName() {
            return name;
        }

        public String  getPhoneNumber() {
            return phoneNumber;
        }

        public String getAddress() {
            return address;
        }


        // Setters

        public void setPhoneNumber(String newPhoneNumber){
        this.phoneNumber = newPhoneNumber;

        }
        public void setAddress(String newAddress) {
            this.address = newAddress;
        }

        public void setName(String newName) {
        this.name = newName;
    }
        // Methods
        public void Details(){
            System.out.println(this.name + " " + "has the following registered number" + " " +  this.phoneNumber + " " + "John Doe has the following registered address " + " " +  this.address);
        }

}